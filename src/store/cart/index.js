export default {
    state: {
        cart:[]
    },
    getters: {
        cart: state => state.cart,
        cartCalcQvyntity: state => state.cart.map(item => item.quantity).reduce((old, newQ) => old + newQ, 0)
        
    },
    mutations: {
        addToCart(state, product){
           let validProductInCart = true
            state.cart = state.cart.filter(item => {
                if(item.product.id == product.id) {
                    item.quantity = item.quantity + 1
                    validProductInCart = false
                }
               return state.cart
            })
            if(validProductInCart) state.cart.push({ product, quantity: 1})
            console.log(state.cart)
        }
    },
    actions:{
        addToCartAction({commit}, product) {
            commit('addToCart', product)
        }
    }

}