import axios from "axios"
export default {
    state: {
        storeProducts:[],
        limit: 9,
        firstValueProduct:0,
        loadMore: true,
        products:[]
    },
    getters: {
        storeProducts: state => state.storeProducts,
        // products: state => state.storeProducts.slice(state.firstValueProduct, state.limit)
        products: state => state.products
    },
    mutations: {
        getProductAll(state, products) {
            localStorage.setItem('products', JSON.stringify(products))
            state.storeProducts = products
            console.log(state.storeProducts)
        },
        getCurrentProducts(state) {
            state.products = state.storeProducts.slice(state.firstValueProduct, state.limit)
        },
        loadMoreProduct(state, getters){
            console.log(getters.products)
            state.firstValueProduct = state.firstValueProduct + state.limit
            state.limit = state.limit + state.limit
            const newLoadProducs = state.storeProducts.slice(state.firstValueProduct, state.limit)
            state.products = [...state.products, ...newLoadProducs]
            console.log(getters.products)
        }
    },
    actions:{
        getProductActions({commit}){
            const storeProduct = localStorage.getItem('products')
            storeProduct ? commit('getProductAll', JSON.parse(storeProduct)) : axios.get(`https://frontend-test.idaproject.com/api/product`).then(res => commit('getProduct', res.data))
            commit("getCurrentProducts")    
    },
        getLoadMoreAction({commit, getters}){
            commit('loadMoreProduct',getters)
        }
    }
}