import { createStore } from "vuex";
import cart from "./cart";
import products from "./products"

const store = {
    namespaced: true,
    modules: {
      cart,
      products
    },


}

export default createStore(store)
