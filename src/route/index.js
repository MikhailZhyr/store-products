import { createRouter, createWebHistory  } from "vue-router";
import MainApp from '@/components/MainApp'
import CartApp from '@/components/CartApp'
const  routes = [
    {
      name: 'main',
      path: "/",
      component: MainApp,
      children: [{
        name: 'cart',
         path: "/cart",
         component: CartApp,
        }]
    },
 
  ]
export default createRouter({
  mode: 'history',
  routes,
  history: createWebHistory(),
});